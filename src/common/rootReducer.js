import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import { responsiveStateReducer } from 'redux-responsive';
import { responsiveDrawer } from 'material-ui-responsive-drawer';
import { enableBatching } from 'redux-batched-actions';
import departmentTree from '../features/department_tree';
import positionTree from '../features/position_tree';
import auth from '../features/auth';
import dialog from '../features/dialogs';
import snackbar from '../features/snackbar';
import app from '../features/app';

const rootReducer = combineReducers({
  browser: responsiveStateReducer,
  responsiveDrawer,
  form: formReducer,
  [departmentTree.constants.NAME]: departmentTree.reducer,
  [positionTree.constants.NAME]: positionTree.reducer,
  [auth.constants.NAME]: auth.reducer,
  dialogs: dialog.reducer,
  snackbar: snackbar.reducer,
  [app.constants.NAME]: app.reducer,
});

export default enableBatching(rootReducer);
